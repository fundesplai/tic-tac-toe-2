import React from 'react';

import styled from 'styled-components';

const DivCelda = styled.div`
    width: 50px;
    height: 50px;
    border: 1px solid black;
    border-radius: 5px;
    text-align: center;
    line-height: 50px;
    font-size: 40px;
    display: inline-block;
    margin: 5px;
`;

export default (props) => {
    let contenido = '-';
    switch (props.valor) {
        case 1:
            contenido = 'X';
            break;
        case 2:
            contenido = 'O';
            break;
        default:
            break;
    }

    return (
        <DivCelda onClick={() => props.clica(props.index)}>{contenido}</DivCelda>
    );

}