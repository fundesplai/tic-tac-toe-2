import React from 'react';

import styled from 'styled-components';

const DivPanel = styled.div`
    width: 190px;
    height: 30px;
    line-height: 30px;
    font-size: 20px;
    text-align: center;
    background-color: blue;
    color: white;
`;


export default (props) => <DivPanel>{props.texto}</DivPanel>