import React, {useState, useEffect} from "react";
import Tablero from './Tablero';
import Panel from './Panel';



export default () => {

  const [mapa, setMapa] = useState([0,0,0, 0,0,0, 0,0,0]);
  const [turno, setTurno] = useState(1);
  const [gameover, setGameOver] = useState(false);


  const clica = (celda) => {
    if (mapa[celda]!==0) return;
    const mapa2 = [...mapa];
    mapa2[celda] = turno;
    setMapa(mapa2);
    setTurno(turno===1 ? 2 : 1 );
  }

  useEffect(() => {
    const zeros = mapa.filter(el => el===0).length;
    if (zeros===0){
      setGameOver(true);
    }
    if (mapa[0] === mapa[1] && mapa[1]===mapa[2]){
      console.log("gana alguien "+mapa[0]);
    }
  }, [mapa])
 
  return (
    <>
      <Panel texto={gameover ? "GAME OVER" : "Jugador "+turno} />
      <Tablero mapa={mapa} clica={clica} />
    </>
  )
};
