import React from 'react';
import Celda from './Celda';

export default (props) => {

    return (
        <>
            <div>
                <Celda index={0} clica={props.clica} valor={props.mapa[0]}/>
                <Celda index={1} clica={props.clica} valor={props.mapa[1]}/>
                <Celda index={2} clica={props.clica} valor={props.mapa[2]}/>
            </div>
            <div>
                <Celda index={3} clica={props.clica} valor={props.mapa[3]}/>
                <Celda index={4} clica={props.clica} valor={props.mapa[4]}/>
                <Celda index={5} clica={props.clica} valor={props.mapa[5]}/>
            </div>
            <div>
                <Celda index={6} clica={props.clica} valor={props.mapa[6]}/>
                <Celda index={7} clica={props.clica} valor={props.mapa[7]}/>
                <Celda index={8} clica={props.clica} valor={props.mapa[8]}/>
            </div>
        </>
    );
}
